-- ****************CREATE DATABASE****************

/*
#-----------------------------------------------#
#                Krlos López                    #
#             ♡ Debian + Gnome ♡                #
#-----------------------------------------------#
*/




/*
 *  COMANDOS PARA POSTGRESQL 
 */

-- ALTER USER <username> WITH PASSWORD '<new_password>';  -- change password
-- ALTER USER <old_username> RENAME TO <new_username>;    -- rename user
-- DROP USER <username>;								  -- remove user


-- *********  CAMBIAR CONTRASEÑA - CHANGE TO PASSWORD  ***********
-- GRANT \du+  -- para ver los usuarios y sus roles
-- sudo -u postgres psql
-- \password postgres
-- pide nueva contraseña ingresarla xD
-- \q
-- listo.












/**************************CARGA MASIVA DESDE CSV**************************/

create or replace procedure cargaMasiva()
language  plpgsql
as  $$ 
begin 
	
	drop table fake;

	create table fake (
	id_fake serial primary key not null,
	NOMBRE_CLIENTE varchar(80) NULL,
	CORREO_CLIENTE varchar(80) NULL,
	CLIENTE_ACTIVO varchar(80) NULL,
	FECHA_CREACION varchar(80) NULL,
	TIENDA_PREFERIDA varchar(80) NULL,
	DIRECCION_CLIENTE varchar(80) NULL,
	CODIGO_POSTAL_CLIENTE varchar(80) NULL,
	CIUDAD_CLIENTE varchar(80) NULL,
	PAIS_CLIENTE varchar(80) NULL,
	FECHA_RENTA varchar(80) NULL,
	FECHA_RETORNO varchar(80) NULL,
	MONTO_A_PAGAR varchar(80) NULL,
	FECHA_PAGO varchar(80) NULL,
	NOMBRE_EMPLEADO varchar(80) NULL,
	CORREO_EMPLEADO varchar(80) NULL,
	EMPLEADO_ACTIVO varchar(80) NULL,
	TIENDA_EMPLEADO varchar(80) NULL,
	USUARIO_EMPLEADO varchar(80) NULL,
	CONTRASEÑA_EMPLEADO varchar(80) NULL,
	DIRECCION_EMPLEADO varchar(80) NULL,
	CODIGO_POSTAL_EMPLEADO varchar(80) NULL,
	CIUDAD_EMPLEADO varchar(80) NULL,
	PAIS_EMPLEADO varchar(80) NULL,
	NOMBRE_TIENDA varchar(80) NULL,
	ENCARGO_TIENDA varchar(80) NULL,
	DIRECCION_TIENDA varchar(80) NULL,
	CODIGO_POSTAL_TIENDA varchar(80) NULL,
	CIUDAD_TIENDA varchar(80) NULL,
	PAIS_TIENDA varchar(80) NULL,
	TIENDA_PELICULA varchar(80) NULL,
	NOMBRE_PELICULA varchar(80) NULL,
	DESCRIPCION_PELICULA varchar(200) NULL,
	AÑO_LANZAMIENTO varchar(80) NULL,
	DIAS_RENTA varchar(80) NULL,
	COSTO_RENTA varchar(80) NULL,
	DURACION varchar(80) NULL,
	COSTO_POR_DAÑO varchar(80) NULL,
	CLASIFICACION varchar(80) NULL,
	LENGUAJE_PELICULA varchar(80) NULL,
	CATEGORIA_PELICULA varchar(80) NULL,
	ACTOR_PELICULA varchar(80) NULL
	);


	-- CARGAR TABLA TAMPORAL
	SET DATESTYLE TO 'European';
	COPY PUBLIC.fake(NOMBRE_CLIENTE, CORREO_CLIENTE, CLIENTE_ACTIVO, FECHA_CREACION, TIENDA_PREFERIDA, DIRECCION_CLIENTE, CODIGO_POSTAL_CLIENTE, CIUDAD_CLIENTE, PAIS_CLIENTE, FECHA_RENTA, FECHA_RETORNO,MONTO_A_PAGAR, FECHA_PAGO, NOMBRE_EMPLEADO, CORREO_EMPLEADO, EMPLEADO_ACTIVO, TIENDA_EMPLEADO, USUARIO_EMPLEADO, CONTRASEÑA_EMPLEADO, DIRECCION_EMPLEADO, CODIGO_POSTAL_EMPLEADO, CIUDAD_EMPLEADO, PAIS_EMPLEADO, NOMBRE_TIENDA, ENCARGO_TIENDA, DIRECCION_TIENDA, CODIGO_POSTAL_TIENDA, CIUDAD_TIENDA, PAIS_TIENDA, TIENDA_PELICULA, NOMBRE_PELICULA, DESCRIPCION_PELICULA, AÑO_LANZAMIENTO, DIAS_RENTA, COSTO_RENTA, DURACION, COSTO_POR_DAÑO, CLASIFICACION, LENGUAJE_PELICULA, CATEGORIA_PELICULA, ACTOR_PELICULA)
	FROM '/home/krlos/Flask App/BD/cargaMasiva.csv' 
	WITH DELIMITER AS ',' NULL AS '-' CSV HEADER;
	
	commit;
end; $$
--call cargaMasiva();	--llamada al procedure
--select count(*) from fake;










  
	
 


-- ================================================================================================================================
-- ================================================================================================================================
--													CONTEO DE DATOS INGRESADOS EN TABLAS
-- ================================================================================================================================
-- ================================================================================================================================


--																		REGISTROS		ERRORES xD
select count(distinct(nombre)) from Direccion;						--	109				603
select count(distinct(pais))  from Ciudad;							--	109
select count(id_clasificacion) from Clasificacion;					--	5
select count(id_lenguaje) from Lenguaje;
select count(id_actor) from Actor;
select count(id_categoria) from Categoria;							--	16
select count(id_tienda) from Tienda;								--	2
select count(id_cliente) from Cliente;								--	599
select count(id_empleado) from Empleado;							--	2
select count(id_empleado) from Jefe_Tienda;
select count(id_pelicula) from Pelicula;							--	1,000
select count(id_tienda) from Inventario;							--	1,521
select count(id_pelicula) from Pelicula_Categoria;					--	1,000
select count(id_pelicula) from Pelicula_Actor;						--	5,462
select count(id_pelicula) from Pelicula_Lenguaje;
select count(id_venta) from Renta;									--	16,045			15963




-- ================================================================================================================================
-- ================================================================================================================================
--													CREAR TABLAS
-- ================================================================================================================================
-- ================================================================================================================================

create or replace procedure eliminartablas()
language  plpgsql
as  $$ 
begin 
	drop table if exists Renta;
	drop table if exists Pelicula_Lenguaje;
	drop table if exists Pelicula_Actor;
	drop table if exists Pelicula_Categoria;
	drop table if exists Inventario;
	drop table if exists Pelicula;
	drop table if exists Jefe_Tienda;
	drop table if exists Empleado;
	drop table if exists Cliente;
	drop table if exists Tienda;
	drop table if exists Categoria;
	drop table if exists Actor;
	drop table if exists Lenguaje;
	drop table if exists Clasificacion;
	drop table if exists Ciudad;
	drop table if exists Direccion;
	commit;
end; $$	
--call eliminartablas ();





create or replace procedure crearBaseDatos_relacion()
language  plpgsql
as  $$ 
begin 
	drop table if exists Renta;
	drop table if exists Pelicula_Lenguaje;
	drop table if exists Pelicula_Actor;
	drop table if exists Pelicula_Categoria;
	drop table if exists Inventario;
	drop table if exists Pelicula;
	drop table if exists Jefe_Tienda;
	drop table if exists Empleado;
	drop table if exists Cliente;
	drop table if exists Tienda;
	drop table if exists Categoria;
	drop table if exists Actor;
	drop table if exists Lenguaje;
	drop table if exists Clasificacion;
	drop table if exists Ciudad;
	drop table if exists Direccion;

	
	create table Direccion(
		id_direccion serial not null primary key,
		nombre varchar(50) not null,
		codigo_postal int null
	);
	
	create table Ciudad(
		id_ciudad serial not null primary key,
		nombre varchar(40) not null,
		pais varchar(40) not null
	);
	
	create table Clasificacion(
		id_clasificacion serial not null primary key,
		nombre varchar(10) not null
	);
	
	create table Lenguaje(
		id_lenguaje serial not null primary key,
		nombre varchar(30) not null
	);
	
	create table Actor(
		id_actor serial not null primary key,
		nombre varchar(30) not null,
		apellido varchar(30) not null
	);
	
	create table Categoria(
		id_categoria serial not null primary key,
		nombre varchar(20) not null
	);
	
	create table Tienda(
		id_tienda serial not null primary key,
		nombre varchar(40) not null,
		id_direccion int not null references Direccion(id_direccion),
		id_ciudad int not null references Ciudad(id_ciudad)
	);
	
	create table Cliente(
		id_cliente serial not null primary key,
		nombre varchar(30) not null,
		apellido varchar(30) not null,
		email varchar(50) not null,
		id_direccion int not null references Direccion(id_direccion),
		fecha_registro date not null,
		activo boolean not null,
		tienda_favorita int null references Tienda(id_tienda),
		id_ciudad int not null references Ciudad(id_ciudad)
	);
	
	
	create table Empleado(
		id_empleado serial not null primary key,
		nombre varchar(30) not null,
		apellido varchar(30) not null,
		id_direccion int not null references Direccion(id_direccion),
		email varchar(50) not null,
		activo boolean not null,
		usuario varchar(20) not null,
		contraseña varchar(60) not null,
		id_ciudad int not null references Ciudad(id_ciudad),
		id_tienda int not null references Tienda(id_tienda)
	);
	
	create table Jefe_Tienda(
		id_empleado int not null references Empleado(id_empleado),
		id_tienda int not null references Tienda(id_tienda),
		primary key(id_empleado,id_tienda)
	);
	
	create table Pelicula(
		id_pelicula serial not null primary key,
		titulo varchar(50) not null,
		descripcion varchar(200) not null,
		año_lanzamiento int not null,
		duracion int not null,
		dias_renta int not null,
		costo_renta decimal(4,2) not null,
		costo_daño decimal(4,2) not null,
		id_clasificacion int not null references Clasificacion(id_clasificacion)
	);
	
	create table Inventario(
		id_tienda int not null references Tienda(id_tienda),
		id_pelicula int not null references Pelicula(id_pelicula),
		primary key(id_tienda,id_pelicula)
	);
	
	create table Pelicula_Categoria(
		id_pelicula int not null references Pelicula(id_pelicula),
		id_categoria int not null references Categoria(id_categoria),
		primary key(id_pelicula,id_categoria)
	);
	
	create table Pelicula_Actor(
		id_pelicula int not null references Pelicula(id_pelicula),
		id_actor int not null references Actor(id_actor),
		primary key(id_pelicula,id_actor)
	);
	
	create table Pelicula_Lenguaje(
		id_pelicula int not null references Pelicula(id_pelicula),
		id_lenguaje int not null references Lenguaje(id_lenguaje),
		primary key(id_pelicula,id_lenguaje)
	);
	
	create table Renta(
		id_venta serial not null primary key,
		monto_pago decimal(4,2) not null,
		fecha_pago timestamp not null,
		fecha_renta timestamp not null,
		fecha_retorno timestamp null,
		id_cliente int not null references Cliente(id_cliente),
		id_empleado int not null references Empleado(id_empleado),
		id_pelicula int not null references Pelicula(id_pelicula),
		id_tienda int not null references Tienda(id_tienda)
	);


	commit;
end; $$	
--call crearbasedatos_relacion() ;





create or replace procedure llenarBaseDatos()
language  plpgsql
as  $$ 
begin 
	----TABLA DIRECCION
	insert into Direccion(nombre,codigo_postal)
	select (direccion_cliente), codigo_postal_cliente::text::int from fake
	where direccion_cliente is not NULL
	group by (direccion_cliente), codigo_postal_cliente::text::int
	UNION
	select (direccion_empleado), codigo_postal_empleado::text::int from fake
	where direccion_empleado is not NULL
	group by (direccion_empleado),codigo_postal_empleado::text::int
	UNION
	select (direccion_tienda), codigo_postal_tienda::text::int from fake
	where direccion_tienda is not NULL
	group by (direccion_tienda),codigo_postal_tienda::text::int;

	-- TABLA CIUDAD
	insert into Ciudad(nombre,pais)
	select lower(ciudad_cliente), lower(pais_cliente) from fake
	where ciudad_cliente is not NULL and pais_cliente is not null
	group by lower(ciudad_cliente), lower(pais_cliente)
	UNION
	select lower(ciudad_empleado), lower(pais_empleado) from fake
	where ciudad_empleado is not NULL and pais_empleado is not null
	group by lower(ciudad_empleado), lower(pais_empleado)
	UNION
	select lower(ciudad_tienda), lower(pais_tienda) from fake
	where ciudad_tienda is not NULL and pais_tienda is not null
	group by lower(ciudad_tienda), lower(pais_tienda);

	--TABLA CLASIFICACIÓN
	insert into clasificacion(nombre)
	select clasificacion 
	from fake
	where clasificacion is not null
	group by clasificacion;

	--TABLA LENGUAJE
	insert into lenguaje(nombre)
	select lenguaje_pelicula from fake
	where lenguaje_pelicula is not null
	group by lenguaje_pelicula;

	-- TABLA ACTOR
	insert into actor(nombre,apellido)
	select split_part(actor_pelicula,' ',1) as nombre, split_part(actor_pelicula,' ',2) as Apellido from fake
	where actor_pelicula is not null and actor_pelicula != '' 
	group by actor_pelicula;

	--TABLA CATEGORIA
	insert into categoria(nombre)
	select categoria_pelicula from fake
	where categoria_pelicula is not null and categoria_pelicula != ''
	group by categoria_pelicula;
	
	-- TABLA TIENDA
	insert into tienda(nombre,id_direccion,id_ciudad)
	select t.nombre_tienda, di.id_direccion, ciu.id_ciudad from fake as t 
	inner join Ciudad as ciu ON ciu.nombre=lower(t.ciudad_tienda) and ciu.pais=lower(t.pais_tienda)
	inner join Direccion as di on di.nombre = (t.direccion_tienda) 
	where t.nombre_tienda is not null and t.direccion_tienda is not null and t.ciudad_tienda is not null and t.pais_tienda is not null
	group by t.nombre_tienda, di.id_direccion, ciu.id_ciudad;
		
	--TABLA CLIENTE
	insert into cliente(nombre, apellido, email, id_direccion, fecha_registro, activo,tienda_favorita,id_ciudad)
	select split_part(t.nombre_cliente,' ',1) as nombre_cliente, split_part(t.nombre_cliente,' ',2) as 
	apellido_cliente, t.correo_cliente, di.id_direccion, t.fecha_creacion::text::timestamp,
	case when t.cliente_activo = 'Si' then TRUE ELSE FALSE END cliente_activo, 
	ti.id_tienda, ciu.id_ciudad from fake as t 
	inner join ciudad as ciu 
	ON ciu.nombre = lower(t.ciudad_cliente) and ciu.pais = lower(t.pais_cliente) 
	inner join tienda as ti 
	ON ti.nombre = t.tienda_preferida
	inner join direccion as di 
	on di.nombre = t.direccion_cliente
	where t.nombre_cliente is not null and t.correo_cliente is not null and t.direccion_cliente is not null and t.fecha_creacion::text::timestamp is not null 
	and t.cliente_activo is not null and t.tienda_preferida is not null and t.ciudad_cliente is not null and t.pais_cliente is not null 
	group by t.nombre_cliente, t.correo_cliente,di.id_direccion,t.fecha_creacion::text::timestamp, t.cliente_activo, t.codigo_postal_cliente, 
	ti.id_tienda, ciu.id_ciudad;

	--TABLA EMPLEADO
	insert into Empleado(nombre,apellido,id_direccion,email,activo,usuario,contraseña,id_ciudad,id_tienda)
	select split_part(t.nombre_empleado,' ',1) as nombre_empleado,split_part(t.nombre_empleado,' ',2) as apellido_empleado, di.id_direccion, t.correo_empleado, case when t.empleado_activo = 'Si' then TRUE ELSE FALSE END empleado_activo,
	t.usuario_empleado, t."contraseÑa_empleado", ciu.id_ciudad, ti.id_tienda 
	from fake as t
	inner join Ciudad as ciu 
	on ciu.nombre = lower(t.ciudad_empleado) and ciu.pais = lower(t.pais_empleado) 
	inner join tienda as ti 
	on ti.nombre=t.tienda_empleado
	inner join Direccion as di 
	on di.nombre = initcap(t.direccion_empleado)
	where t.nombre_empleado is not null and t.direccion_empleado is not null and t.correo_empleado is not null and t.cliente_activo is not null and
	t.usuario_empleado is not null and t."contraseÑa_empleado" is not null and t.ciudad_empleado is not null and t.pais_empleado is not null and 
	t.tienda_empleado is not null 
	group by t.nombre_empleado, di.id_direccion, t.correo_empleado, t.empleado_activo, t.usuario_empleado, t."contraseÑa_empleado", t.codigo_postal_empleado,
	ciu.id_ciudad, ti.id_tienda;
		
	--TABLA JEFE_TIENDA
	insert into jefe_tienda(id_empleado,id_tienda)
	select em.id_empleado, em.id_tienda 
	from Empleado as em
	inner join fake as t 
	on em.nombre = split_part(t.encargo_tienda,' ',1) and em.apellido = split_part(t.encargo_tienda,' ',2) 
	where t.encargo_tienda is not null
	group by em.id_empleado, em.id_tienda;

	--TABLA PELICULA
	insert into pelicula(titulo,descripcion,año_lanzamiento,duracion,dias_renta,costo_renta,costo_daño,id_clasificacion)
	select t.nombre_pelicula, t.descripcion_pelicula, t."aÑo_lanzamiento"::text::int,t.duracion::text::int,t.dias_renta::text::int, t.costo_renta::text::decimal, t."costo_por_daÑo"::text::decimal, 
	cla.id_clasificacion from fake as t
	inner join Clasificacion as cla on cla.nombre = t.clasificacion
	where t.nombre_pelicula is not null and t.descripcion_pelicula is not null
	and t."aÑo_lanzamiento" is not null and t.duracion is not null and t.dias_renta is not null and t.costo_renta is not null 
	and t."costo_por_daÑo" is not null and t.clasificacion is not null
	group by t.nombre_pelicula,t.descripcion_pelicula, t."aÑo_lanzamiento",t.duracion,t.dias_renta,t.costo_renta, t."costo_por_daÑo",
	cla.id_clasificacion;

	--TABLA INVENTARIO
	insert into Inventario(id_tienda,id_pelicula)
	select ti.id_tienda, pe.id_pelicula from fake as t
	inner join Tienda as ti on ti.nombre=t.tienda_pelicula 
	inner join Pelicula as pe on pe.titulo = t.nombre_pelicula
	where t.tienda_pelicula is not null and t.nombre_pelicula is not null
	group by ti.id_tienda, pe.id_pelicula;

	--TABLA PELICULA_CATEGORIA
	insert into Pelicula_Categoria(id_pelicula,id_categoria)
	select pe.id_pelicula, cat.id_categoria from fake as t
	inner join pelicula as pe on pe.titulo = t.nombre_pelicula
	inner join categoria as cat on cat.nombre = t.categoria_pelicula 
	where t.nombre_pelicula is not null and t.categoria_pelicula is not null
	group by pe.id_pelicula, cat.id_categoria;

	--TABLA PELICULA_ACTOR
	insert into Pelicula_Actor(id_pelicula,id_actor)
	select pe.id_pelicula, act.id_actor from fake as t
	inner join pelicula as pe on pe.titulo = t.nombre_pelicula 
	inner join actor as act on act.nombre = split_part(t.actor_pelicula,' ',1) and act.apellido = split_part(t.actor_pelicula,' ',2)
	where t.nombre_pelicula is not null and t.actor_pelicula is not null
	group by pe.id_pelicula, act.id_actor;

	--TABLA PELICULA_LENGUAJE
	insert into Pelicula_Lenguaje(id_pelicula,id_lenguaje)
	select pe.id_pelicula,len.id_lenguaje from fake as t
	inner join pelicula as pe on pe.titulo = t.nombre_pelicula
	inner join Lenguaje as len on len.nombre = t.lenguaje_pelicula
	where t.nombre_pelicula is not null and t.lenguaje_pelicula is not null
	group by pe.id_pelicula, len.id_lenguaje;

	--TABLA RENTA
	insert into Renta(monto_pago,fecha_pago,fecha_renta,fecha_retorno,id_cliente,id_empleado,id_pelicula,id_tienda)
	select t.monto_a_pagar::text::decimal, t.fecha_pago::text::timestamp, t.fecha_renta::text::timestamp, t.fecha_retorno::text::timestamp, cli.id_cliente, em.id_empleado, pe.id_pelicula, ti.id_tienda from fake as t
	inner join cliente as cli on cli.nombre = split_part(t.nombre_cliente,' ',1) and cli.apellido = split_part(t.nombre_cliente,' ',2) 
	inner join empleado as em on em.nombre = split_part(t.nombre_empleado,' ',1) and em.apellido = split_part(t.nombre_empleado,' ',2)
	inner join pelicula as pe on pe.titulo = t.nombre_pelicula 
	inner join tienda as ti on ti.nombre = t.tienda_pelicula 
	where t.fecha_pago is not null and t.fecha_renta is not null and t.nombre_cliente is not null 
	and t.nombre_empleado is not null and t.nombre_pelicula is not null and t.tienda_pelicula is not null
	group by t.monto_a_pagar, t.fecha_pago, t.fecha_renta, t.fecha_retorno, cli.id_cliente, em.id_empleado, pe.id_pelicula, ti.id_tienda;

	commit;
end; $$
-- call llenarBaseDatos();
		
				

-- ================================================================================================================================
-- ================================================================================================================================
--													CONSULTAS REPORTERIA
-- ================================================================================================================================
-- ================================================================================================================================

------------------------------------------------------- REPORTERIA ---------------------------------------------


--NO ME SALIO XD
/*create or replace procedure consulta1()
	RETURNS table (cantidad_copias integer)--definicion de la estructura de la tabla a devolver
	as  $$ 
	begin 
		return QUERY
		select count(titulo) as cantidad_copias 
		from inventario as inv
		inner join pelicula as pe on pe.id_pelicula = inv.id_pelicula
		where pe.titulo = 'SUGAR WONKA';
	end; 
$$ language  plpgsql;
call consulta1();*/


--Consulta 1     - malo debe dar 37 y son 17
	select count(titulo) as cantidad_copias 
	from inventario as inv
	inner join pelicula as pe on pe.id_pelicula = inv.id_pelicula
	where pe.titulo = 'SUGAR WONKA';




--Consulta 2
WITH
	veces_cliente AS (
		select id_cliente, count(id_cliente) as veces_a_rentado, sum(monto_pago) as pago_total from renta
		group by id_cliente
	), veces_mayor_40 AS (
		select id_cliente, veces_a_rentado, pago_total from veces_cliente
		where veces_a_rentado >= 40
	) -- ya viene agrupado
SELECT cli.nombre, cli.apellido, veces_a_rentado, pago_total from veces_mayor_40 as veces
inner join cliente as cli on cli.id_cliente = veces.id_cliente;



--Consulta 3
select (nombre || ' ' || apellido) AS apellidos_con_son from Actor
where strpos(upper(apellido),'SON')>0
order by (nombre || ' ' || apellido);



--Consulta 4
select act.nombre, act.apellido, pe.año_lanzamiento from Pelicula_Actor as pe_ac
inner join Actor as act on pe_ac.id_actor = act.id_actor
inner join Pelicula as pe on pe_ac.id_pelicula = pe.id_pelicula
where strpos(lower(pe.descripcion),'crocodile')>0 and strpos(lower(pe.descripcion),'shark')>0
order by act.apellido;


--Consulta 5
WITH
	clientes_rentado as (
		select id_cliente, count(id_cliente) as total from Renta
		group by id_cliente
	), mas_rentado as ( -- tengo el id del cliente que más rento
		select id_cliente, count(id_cliente) as maximo from Renta
		group by id_cliente
		having count(id_cliente) = (select max(total) from clientes_rentado)
	), pais_del_masrentado as ( -- tengo el pais del cliente que más rento
		select ciu.pais from mas_rentado as mr
		inner join Cliente as cli on cli.id_cliente = mr.id_cliente
		inner join Ciudad as ciu on ciu.id_ciudad = cli.id_ciudad
	),
	cantidad_rentas_enpais as ( -- TOTAL de rentas en el pais
		select count(re.id_venta) as total_renta_pais from Renta as re
		inner join Cliente as cli on re.id_cliente = cli.id_cliente 
		inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad
		where ciu.pais = (select pmr.pais from pais_del_masrentado as pmr)
	),
	porcentaje_clienteMaximo_enPais as (
		select (mr.maximo*100/ crp.total_renta_pais ) as porcentaje_en_pais from mas_rentado as mr, cantidad_rentas_enpais as crp
	),
	cliente_mas_rentado as (
		select cli.nombre, cli.apellido, ciu.pais from mas_rentado as mr
		inner join cliente as cli on mr.id_cliente = cli.id_cliente 
		inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad
	)
	select * from cliente_mas_rentado, porcentaje_clienteMaximo_enPais;
	

-- Consulta 6
WITH 
	total_clientes_paises as (
		select ciu.pais, count(cli.id_cliente) as total_clientes_pais from cliente as cli 
		inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad
		group by ciu.pais
	), total_clientes_ciudad as (
		select ciu.pais, ciu.nombre, count(cli.id_cliente) as total_clientes_ciudad from Cliente as cli
		inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad
		group by ciu.pais, ciu.nombre
	), porcentaje_ciudad as (
		select tcp.pais, tcc.nombre as ciudad, round((tcc.total_clientes_ciudad * 100::float / tcp.total_clientes_pais::float)::numeric,2) as porcentaje_ciudad from total_clientes_ciudad as tcc
		inner join total_clientes_paises as tcp on tcc.pais = tcp.pais
	)
	select tcc.total_clientes_ciudad, tcp.total_clientes_pais, pc.porcentaje_ciudad, pc.ciudad as ciudad, pc.pais from porcentaje_ciudad as pc
	inner join total_clientes_ciudad as tcc on pc.ciudad = tcc.nombre and pc.pais = tcc.pais
	inner join total_clientes_paises as tcp on pc.pais = tcp.pais;


-- Consulta 7
WITH 
	total_ciudades_porPais as (
		select pais, count(id_ciudad) as no_ciudades_porPais from Ciudad as ciu
		group by pais order by pais
	), total_rentas_porCiudad as (
		select ciu.pais, ciu.nombre, count(re.id_venta) as total_rentas_porCiudad from Renta as re
		inner join Cliente as cli on re.id_cliente = cli.id_cliente 
		inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad
		group by ciu.pais, ciu.nombre
	), ciudad_promedios as (
		select trp.pais, trp.nombre, round((trp.total_rentas_porCiudad::float/tcp.no_ciudades_porPais::float)::numeric,2) as promedio_rentas_ciudad from total_rentas_porCiudad as trp
		inner join total_ciudades_porPais as tcp on trp.pais = tcp.pais
		group by trp.pais, trp.nombre, promedio_rentas_ciudad 
	)
	select * from ciudad_promedios order by pais;


--Consulta 8
WITH 
	total_rentas_porPais as(
		select ciu.pais, count(re.id_venta) as total_rentas from Renta as re
		inner join Cliente as cli on re.id_cliente = cli.id_cliente 
		inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad
		group by ciu.pais
	), rentas_sports_porPais as (
		select ciu.pais, count(re.id_venta) as rentas_sports from Renta as re
		inner join Cliente as cli on re.id_cliente = cli.id_cliente 
		inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad
		inner join Pelicula as pe on re.id_pelicula = pe.id_pelicula
		inner join Pelicula_Categoria as p_c on pe.id_pelicula = p_C.id_pelicula 
		inner join Categoria as cat on p_c.id_categoria = cat.id_categoria
		where cat.nombre = 'Sports'
		group by ciu.pais
	), porcentaje_porPais as (
		select rsp.pais, round(((rsp.rentas_sports * 100)::float/trp.total_rentas::float)::numeric,2) as porcentaje_sports from rentas_sports_porPais as rsp
		inner join total_rentas_porPais as trp on rsp.pais = trp.pais
	) 
	select * from porcentaje_porPais;
	

-- Consulta 9
WITH
	total_rentas_dayton as (
		select ciu.nombre, count(re.id_venta) rentas from Renta as re
		inner join Cliente as cli on re.id_cliente = cli.id_cliente 
		inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad 
		where ciu.nombre = 'dayton'
		group by ciu.nombre
	), lista_ciudades_USA as (
		select ciu.nombre as ciudad, count(id_venta) as rentas from Renta as re
		inner join Cliente as cli on re.id_cliente = cli.id_cliente 
		inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad 
		where ciu.pais = 'united states'
		group by ciu.nombre
	), lista_ciudades_USA_mayor_dyton as (
		select lista.* from lista_ciudades_USA as lista, total_rentas_dayton
		where lista.rentas > total_rentas_dayton.rentas
	)
	select * from lista_ciudades_USA_mayor_dyton;


-- Consulta 10
wITH 
	rentas_porcategoria_porCiudad as (
		select ciu.pais,ciu.nombre as ciudad ,cat.nombre as categoria, count(re.id_venta) as rentas from Renta as re
		inner join Cliente as cli on re.id_cliente = cli.id_cliente 
		inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad 
		inner join Pelicula as peli on re.id_pelicula = peli.id_pelicula
		inner join Pelicula_Categoria as pc on peli.id_pelicula = pc.id_pelicula 
		inner join Categoria as cat on pc.id_categoria = cat.id_categoria
		group by ciu.pais,ciu.nombre,cat.nombre
		order by ciu.pais,ciu.nombre,count(re.id_venta) desc
	), solo_tops as (
		select tops_porcategoria_porCiudad.* from 
		(select rpp.*, 
		 rank() over (partition by rpp.pais, rpp.ciudad order by rpp.rentas desc) 
		from rentas_porcategoria_porCiudad as rpp) tops_porcategoria_porCiudad
		where rank = 1
	) 
	select st.pais,st.ciudad,st.categoria, st.rentas from solo_tops as st where categoria = 'Horror';
			



