from flask import Flask,request,jsonify #pip install Flask
import psycopg2  #pip install psycopg2
from psycopg2.extras import RealDictCursor
from flask_cors import CORS #pip install flask-cors


#************ COMANDOS PARA EJECUTAR EL ARCHIVO ***********
#   export FLASK_APP=app.py
#   export FLASK_ENV=development
#   flask run
#**********************************************************



#-----------------------------------------------#
#                 Krlos López                   #
#             ♡ Debian + Gnome ♡                #
#-----------------------------------------------#




app = Flask(__name__)
CORS(app)

def conexion():
    return psycopg2.connect(
    host="localhost",
    database="postgres",
    user="postgres",
    password="root")


# 
# DEBIDO A LA PRISA DE LA ENTREGA DE LA PRACTICA REALICE ESTO LO CUAL NO ES RECOMENDABLE 
# ESCRIBIR LA CONSULTA COMO LO REALICE EN ESTA PRACTICA
# DEBIDO A QUE NO ME SALIO EL PROCEDURE DEL LADO DE LA BD NO ME COMPLIQUE ASI QUE LO COLOQUE TODO AKI   xD
#


@app.route('/consulta1', methods=['GET'])
def consulta1():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" select count(titulo) as cantidad_copias from inventario as inv inner join pelicula as pe on pe.id_pelicula = inv.id_pelicula where pe.titulo = 'SUGAR WONKA'; ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)




@app.route('/consulta2', methods=['GET'])
def consulta2():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" WITH veces_cliente AS ( select id_cliente, count(id_cliente) as veces_a_rentado, sum(monto_pago) as pago_total from renta group by id_cliente ), veces_mayor_40 AS ( select id_cliente, veces_a_rentado, pago_total from veces_cliente where veces_a_rentado >= 40 ) SELECT cli.nombre, cli.apellido, veces_a_rentado, pago_total from veces_mayor_40 as veces inner join cliente as cli on cli.id_cliente = veces.id_cliente; ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)





@app.route('/consulta3', methods=['GET'])
def consulta3():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" select (nombre || ' ' || apellido) AS apellidos_con_son from Actor where strpos(upper(apellido),'SON')>0 order by (nombre || ' ' || apellido); ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)





@app.route('/consulta4', methods=['GET'])
def consulta4():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" select act.nombre, act.apellido, pe.año_lanzamiento from Pelicula_Actor as pe_ac inner join Actor as act on pe_ac.id_actor = act.id_actor inner join Pelicula as pe on pe_ac.id_pelicula = pe.id_pelicula where strpos(lower(pe.descripcion),'crocodile')>0 and strpos(lower(pe.descripcion),'shark')>0 order by act.apellido; ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)




@app.route('/consulta5', methods=['GET'])
def consulta5():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" WITH clientes_rentado as ( select id_cliente, count(id_cliente) as total from Renta group by id_cliente ), mas_rentado as ( select id_cliente, count(id_cliente) as maximo from Renta group by id_cliente having count(id_cliente) = (select max(total) from clientes_rentado) ), pais_del_masrentado as ( select ciu.pais from mas_rentado as mr inner join Cliente as cli on cli.id_cliente = mr.id_cliente inner join Ciudad as ciu on ciu.id_ciudad = cli.id_ciudad ), cantidad_rentas_enpais as ( select count(re.id_venta) as total_renta_pais from Renta as re inner join Cliente as cli on re.id_cliente = cli.id_cliente inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad where ciu.pais = (select pmr.pais from pais_del_masrentado as pmr) ), porcentaje_clienteMaximo_enPais as ( select (mr.maximo*100/ crp.total_renta_pais ) as porcentaje_en_pais from mas_rentado as mr, cantidad_rentas_enpais as crp ), cliente_mas_rentado as ( select cli.nombre, cli.apellido, ciu.pais from mas_rentado as mr inner join cliente as cli on mr.id_cliente = cli.id_cliente inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad ) select * from cliente_mas_rentado, porcentaje_clienteMaximo_enPais; ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)



@app.route('/consulta6', methods=['GET'])
def consulta6():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" WITH  total_clientes_paises as ( select ciu.pais, count(cli.id_cliente) as total_clientes_pais  from cliente as cli  inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad group by ciu.pais order by ciu.pais asc ), total_clientes_ciudad as ( select ciu.pais, ciu.nombre, count(cli.id_cliente) as total_clientes_ciudad  from Cliente as cli inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad group by ciu.pais, ciu.nombre ), porcentaje_ciudad as ( select tcp.pais, tcc.nombre as ciudad, round((tcc.total_clientes_ciudad * 100::float / tcp.total_clientes_pais::float)::numeric,2) as porcentaje_ciudad  from total_clientes_ciudad as tcc inner join total_clientes_paises as tcp on tcc.pais = tcp.pais ) select pc.pais, pc.ciudad as ciudad, pc.porcentaje_ciudad, tcc.total_clientes_ciudad, tcp.total_clientes_pais  from porcentaje_ciudad as pc inner join total_clientes_ciudad as tcc on pc.ciudad = tcc.nombre and pc.pais = tcc.pais inner join total_clientes_paises as tcp on pc.pais = tcp.pais; ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)




@app.route('/consulta7', methods=['GET'])
def consulta7():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" WITH total_ciudades_porPais as ( select pais, count(id_ciudad) as no_ciudades_porPais from Ciudad as ciu group by pais order by pais ), total_rentas_porCiudad as ( select ciu.pais, ciu.nombre, count(re.id_venta) as total_rentas_porCiudad from Renta as re inner join Cliente as cli on re.id_cliente = cli.id_cliente inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad group by ciu.pais, ciu.nombre ), ciudad_promedios as ( select trp.pais, trp.nombre, round((trp.total_rentas_porCiudad::float/tcp.no_ciudades_porPais::float)::numeric,2) as promedio_rentas_ciudad from total_rentas_porCiudad as trp inner join total_ciudades_porPais as tcp on trp.pais = tcp.pais group by trp.pais, trp.nombre, promedio_rentas_ciudad ) select * from ciudad_promedios order by pais; ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)




@app.route('/consulta8', methods=['GET'])
def consulta8():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" WITH  total_rentas_porPais as( select ciu.pais, count(re.id_venta) as total_rentas from Renta as re inner join Cliente as cli on re.id_cliente = cli.id_cliente  inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad group by ciu.pais order by ciu.pais asc  ), rentas_sports_porPais as ( select ciu.pais, count(re.id_venta) as rentas_sports from Renta as re inner join Cliente as cli on re.id_cliente = cli.id_cliente  inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad inner join Pelicula as pe on re.id_pelicula = pe.id_pelicula inner join Pelicula_Categoria as p_c on pe.id_pelicula = p_C.id_pelicula  inner join Categoria as cat on p_c.id_categoria = cat.id_categoria where cat.nombre = 'Sports' group by ciu.pais ), porcentaje_porPais as ( select rsp.pais, round(((rsp.rentas_sports * 100)::float/trp.total_rentas::float)::numeric,2) as porcentaje_sports from rentas_sports_porPais as rsp inner join total_rentas_porPais as trp on rsp.pais = trp.pais )  select * from porcentaje_porPais;  ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)




@app.route('/consulta9', methods=['GET'])
def consulta9():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" WITH total_rentas_dayton as ( select ciu.nombre, count(re.id_venta) rentas from Renta as re inner join Cliente as cli on re.id_cliente = cli.id_cliente inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad where ciu.nombre = 'dayton' group by ciu.nombre ), lista_ciudades_USA as ( select ciu.nombre as ciudad, count(id_venta) as rentas from Renta as re inner join Cliente as cli on re.id_cliente = cli.id_cliente inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad where ciu.pais = 'united states' group by ciu.nombre ), lista_ciudades_USA_mayor_dyton as ( select lista.* from lista_ciudades_USA as lista, total_rentas_dayton where lista.rentas > total_rentas_dayton.rentas ) select * from lista_ciudades_USA_mayor_dyton;  ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)




@app.route('/consulta10', methods=['GET'])
def consulta10():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" wITH rentas_porcategoria_porCiudad as ( select ciu.pais,ciu.nombre as ciudad ,cat.nombre as categoria, count(re.id_venta) as rentas from Renta as re inner join Cliente as cli on re.id_cliente = cli.id_cliente inner join Ciudad as ciu on cli.id_ciudad = ciu.id_ciudad inner join Pelicula as peli on re.id_pelicula = peli.id_pelicula inner join Pelicula_Categoria as pc on peli.id_pelicula = pc.id_pelicula inner join Categoria as cat on pc.id_categoria = cat.id_categoria group by ciu.pais,ciu.nombre,cat.nombre order by ciu.pais,ciu.nombre,count(re.id_venta) desc ), solo_tops as ( select tops_porcategoria_porCiudad.* from  (select rpp.*, rank() over (partition by rpp.pais, rpp.ciudad order by rpp.rentas desc) from rentas_porcategoria_porCiudad as rpp) tops_porcategoria_porCiudad where rank = 1 ) select st.pais,st.ciudad,st.categoria, st.rentas from solo_tops as st where categoria = 'Horror'; ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)






@app.route('/eliminarDatosTemporal', methods=['GET'])
def eliminarTemporal():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" drop from fake; ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)



@app.route('/eliminarModelo', methods=['GET'])
def eliminarModelo():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" call eliminartablas (); ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)



@app.route('/cargarTemporal', methods=['GET'])
def cargarTemporal():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" call cargaMasiva(); ")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)



@app.route('/cargarModelo', methods=['GET'])
def cargarModelo():
    conn = conexion()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(" call crearBaseDatos_relacion();  call llnarBaseDatos();")
    rows = cur.fetchall()
    conn.close()
    return jsonify(rows)













if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
