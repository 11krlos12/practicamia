# Manejo e Implementación de Archivos - Practica 1
# USAC - GUATEMALA

Consultas a través de una API, utilizando flask

# Recursos
* [Recursos](https://www.w3schools.com/sql/)
* [Flask](https://www.youtube.com/watch?v=82xc4hnZQeU)
    * #### Instalación de [Python3](https://www.python.org/downloads/) o bien sigue el siguiente [tutorial](https://www.youtube.com/watch?v=nGhP8NmnCyU)
        * En mi caso ya venia instalado, si no lo tienes instalado lo puedes descargar a través de su [pagina](https://www.python.org/downloads/) o con el comando: *apt-get install python3*
        * Comprueba que lo tengas instalado ejecutando el comando en tu terminal: *python3 --version*
        <div><p style = 'text-align:center;'><img src="./img/python-version.png" alt="JuveYell" width="300px"></p></div>
        

    * #### Instalación de [Python Pip 3](https://www.youtube.com/watch?v=j6pj8O0E9Zs)
        * Ejecutar el siguiente comando: *apt-get install python3-pip*
        * Ver la versión instalada: *pip3 --version*
        * Instalar el paquete: *pip3 install opencv-python*
            * Para validar dicho paquete ejecutar los siguientes comandos:
                * *python3*
                    * *import cv2*
                    * *cv2.__version__*
                    * **¡...SALIR DE LA TERMNINAL DE PYTHON...!**


    * #### Instalación de [Flask](https://www.youtube.com/watch?v=82xc4hnZQeU)
        * Tener instalado: 
            * python3:  *python3 --version*
            * pip3:     *pip3 --version*
        * Crear un directorio donde alvergara nuestro proyecto, **usare un ejemplo**:
            * Directorio donde alvergare mi practica: "/home/krlos/Flask App/"
            * *mkdir practica*
            * *cd practica*
            * *python3 -m venv venv* (al ejecutar este comando crea archivos iniciales necesarios para poder ejecutar flask)
            * *source venv/bin/activate* (activación del entorno virtual)
            * *pip3 install flask*
            * *flask --version*  (valir instalación)

* Escribir lo siguiente en el archivo App.py
<div>
<p style = 'text-align:center;'>
<img src="./img/levantar-flask.png" alt="JuveYell" width="500px">
</p>
</div>

* Comando para levantar el servicio
    * *export FLASK_APP=app.py*
    * *export FLASK_ENV=development*
    * *flask run*
<div>
<p style = 'text-align:center;'>
<img src="./img/levantado.png" alt="JuveYell" width="400px">
</p>
</div>

* Para los endpoint de la practica se accede de la siguiente forma:
    * http://127.0.0.1:5000/consulta1...10
<div>
<p style = 'text-align:center;'>
<img src="./img/endpoint.png" alt="JuveYell" width="220px">
</p>
</div>    
    



# Sistema Operativo - [Debian 11](https://www.debian.org/)
<div>
<p style = 'text-align:center;'>
<img src="./img/so.png" alt="JuveYell" width="500px">
</p>
</div>


# Base de Datos
* Administrador de base de datos -Cliente- [DBeaver](https://dbeaver.io/) el cual puede manipular varias bases de datos. 
* Se utilizo [PostgreSQL 12](https://www.postgresql.org/download/linux/debian/#apt), la instalación del cliente queda a discreción.
* Para obtener la ultima versión de PostgreSQL ir a [Wiki](https://wiki.postgresql.org/wiki/Apt).

<div>
<p style = 'text-align:center;'>
<img src="./img/dbeaver-version.png" alt="JuveYell" width="300px">
<img src="./img/postgresql.png" alt="JuveYell" width="300px">
</p>
</div>


# Extras
* **Lenguaje de Definición de Datos (DDL):** Comandos para crear y definir nuevas base de datos, tablas, campos e índices.

| Comando mas comunes|
|:---:	|
|**CREATE**|
|**DROP**|
|**ALTER**|
|**MODIFY**|
|**TRUNCATE**|
|**COMMENT**|

* **Lenguaje de Manipulación de Datos (DML):** Comandos para insertar, modificar y eliminar registros, así como ordenar, filtrar y extraer información de la base de datos.

| Comando mas comunes|
|:---:	|
|**INSERT**|
|**UPDATE**|
|**DELETE**|
|**SELECT**|




